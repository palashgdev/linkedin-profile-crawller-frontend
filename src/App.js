import React, { useState, useCallback } from "react";
import {
  Button,
  IconButton,
  AppBar,
  Toolbar,
  Card,
  CardContent,
  Typography,
  CardActionArea,
  TextField,
  CircularProgress,
  List,
  ListItemAvatar,
  Avatar,
  ListItem,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";

function App() {
  const [sessionCookie, setSessionCookie] = useState(
    "AQEDASKMCugAuJMeAAABcmluyccAAAFyjXtNx1YAXF5029AvDKpsmsz_LZ0VstuL-G8J5-yfwsgjo_DLXEm52HKyUco0LoR0LsiZ16G-FD9cU2LMCdGeTQiA0bHDuOe7DI6WM1l2szwyKwLfKC29WI4U"
  );
  const [linkedinUrl, setLinkedinUrl] = useState(
    "https://www.linkedin.com/in/deepansh946/"
  );
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);

  const [profile, setProfile] = useState();
  const [education, setEducation] = useState();
  const [skills, setSkills] = useState();
  const [volunteer, setVolunteer] = useState();
  const [experiences, setExperiences] = useState();

  const onSubmitHandler = useCallback(
    async (e) => {
      e.preventDefault();
      setLoading(true);

      const response = await fetch("http://localhost:8000/api/profile", {
        body: JSON.stringify({
          sessionCookie,
          keepAlive: true,
          timeout: 20000,
          userAgent:
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36",
          headless: true,
          linkedinUrl,
        }),
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
      });

      const { statusCode, payload } = await response.json();
      if (statusCode === 200) {
        setProfile(payload.profile);
        setEducation(payload.education);
        setSkills(payload.skills);
        setVolunteer(payload.volunteer);
        setExperiences(payload.experiences);
        setLoading(false);
        setError(false);
        setSuccess(true);
      } else {
        setLoading(false);
        setError(true);
        setSuccess(false);
      }
    },
    [linkedinUrl, sessionCookie]
  );
  const onChangeHandler = (val, func) => {
    func(val);
  };
  return (
    <div className="App">
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          minHeight: "90vh",
        }}
      >
        <Card
          style={{
            display: "flex",
            flexDirection: "column",
            minWidth: "60%",
            minHeight: "40vh",
          }}
        >
          <CardContent style={{ flex: 1 }}>
            {loading === false && success === false && error === false && (
              <>
                <Typography gutterBottom variant="h5" component="h2">
                  Linkedin Profile Crawler
                </Typography>
                <form
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    flexDirection: "column",
                    minHeight: "30vh",
                  }}
                  onSubmit={onSubmitHandler}
                >
                  <div>
                    <TextField
                      label="Session Cookie"
                      fullWidth
                      lang="eng"
                      required
                      onChange={(e) =>
                        onChangeHandler(e.target.value, setSessionCookie)
                      }
                      value={sessionCookie}
                      type="text"
                    />
                    <TextField
                      value={linkedinUrl}
                      label="Linkedin URL"
                      fullWidth
                      required
                      lang="eng"
                      onChange={(e) =>
                        onChangeHandler(e.target.value, setLinkedinUrl)
                      }
                      type="text"
                    />
                  </div>
                  <CardActionArea>
                    <Button type="submit" size="small" color="primary">
                      Submit
                    </Button>
                  </CardActionArea>
                </form>
              </>
            )}

            {loading === true && success === false && error === false && (
              <CircularProgress />
            )}

            {loading === false && success === false && error === true && (
              <Typography>Error While Fetching the Data</Typography>
            )}

            {loading === false && success === true && error === false && (
              <>
                <Card>
                  <CardContent>
                    <Typography variant="h3" gutterBottom>
                      Profile
                    </Typography>

                    <List>
                      <ListItemAvatar
                        style={{
                          display: "flex",
                          justifyContent: "center",
                        }}
                      >
                        <Avatar alt="profile" src={profile.photo} />
                      </ListItemAvatar>
                    </List>
                    <List>
                      <ListItem>
                        <Typography>FullName: {profile.fullName}</Typography>
                      </ListItem>
                      <ListItem>
                        <Typography>Title: {profile.title}</Typography>
                      </ListItem>
                      <ListItem>
                        <Typography>Location: {profile.location}</Typography>
                      </ListItem>
                      <ListItem>
                        <Typography>
                          Description: {profile.description}
                        </Typography>
                      </ListItem>
                    </List>
                  </CardContent>
                </Card>

                <Card
                  style={{
                    marginTop: "20px",
                  }}
                >
                  <CardContent>
                    <Typography variant="h3" gutterBottom>
                      Education
                    </Typography>
                    {education.map(
                      ({
                        schoolName,
                        degreeName,
                        fieldOfStudy,
                        durationInDays,
                        startDate,
                        endDate,
                      }) => {
                        const startDates = new Date(startDate);
                        const endDates = new Date(endDate);

                        return (
                          <>
                            <ListItem>
                              <Typography>SchoolName: {schoolName}</Typography>
                            </ListItem>
                            <ListItem>
                              <Typography>Degree: {degreeName}</Typography>
                            </ListItem>
                            <ListItem>
                              <Typography>
                                Field Of Study: {fieldOfStudy}
                              </Typography>
                            </ListItem>
                            <ListItem>
                              <Typography>Days: {durationInDays}</Typography>
                            </ListItem>
                            <ListItem>
                              Start Date :
                              {`${startDates.getDay()}/${startDates.getMonth()}/${startDates.getFullYear()}`}
                            </ListItem>
                            <ListItem>
                              End Date :
                              {`${endDates.getDay()}/${endDates.getMonth()}/${endDates.getFullYear()}`}
                            </ListItem>
                          </>
                        );
                      }
                    )}
                  </CardContent>
                </Card>

                <Card
                  style={{
                    marginTop: "20px",
                  }}
                >
                  <CardContent>
                    <Typography variant="h3" gutterBottom>
                      Skills
                    </Typography>
                    {skills.map(({ skillName, endorsementCount }) => (
                      <ListItem>
                        <Typography>
                          {skillName}:{endorsementCount}
                        </Typography>
                      </ListItem>
                    ))}
                  </CardContent>
                </Card>

                <Card
                  style={{
                    marginTop: "20px",
                  }}
                >
                  <CardContent>
                    <Typography variant="h3" gutterBottom>
                      Volunteer
                    </Typography>
                    {volunteer.map(
                      ({ title, company, startDate, endDate, description }) => {
                        const startDates = new Date(startDate);
                        const endDates = new Date(endDate);
                        return (
                          <>
                            <ListItem>
                              <Typography>Title: {title}</Typography>
                            </ListItem>
                            <ListItem>
                              <Typography>Company: {company}</Typography>
                            </ListItem>
                            <ListItem>
                              <Typography>
                                Description: {description}
                              </Typography>
                            </ListItem>
                            <ListItem>
                              Start Date :
                              {`${startDates.getDay()}/${startDates.getMonth()}/${startDates.getFullYear()}`}
                            </ListItem>
                            <ListItem>
                              End Date :
                              {`${endDates.getDay()}/${endDates.getMonth()}/${endDates.getFullYear()}`}
                            </ListItem>
                          </>
                        );
                      }
                    )}
                  </CardContent>
                </Card>

                <Card
                  style={{
                    marginTop: "20px",
                  }}
                >
                  <CardContent>
                    <Typography variant="h3" gutterBottom>
                      Experiences
                    </Typography>
                    {experiences.map(
                      ({
                        title,
                        company,
                        description,
                        startDate,
                        endDate,
                        durationInDays,
                      }) => {
                        const startDates = new Date(startDate);
                        const endDates = new Date(endDate);
                        return (
                          <Card>
                            <CardContent>
                              <ListItem>
                                <Typography>Title: {title}</Typography>
                              </ListItem>
                              <ListItem>
                                <Typography>Company: {company}</Typography>
                              </ListItem>
                              <ListItem>
                                <Typography>
                                  Description: {description}
                                </Typography>
                              </ListItem>
                              <ListItem>
                                <Typography>
                                  No of Days :{durationInDays}
                                </Typography>
                              </ListItem>
                              <ListItem>
                                Start Date :
                                {`${startDates.getDay()}/${startDates.getMonth()}/${startDates.getFullYear()}`}
                              </ListItem>
                              <ListItem>
                                End Date :
                                {`${endDates.getDay()}/${endDates.getMonth()}/${endDates.getFullYear()}`}
                              </ListItem>
                            </CardContent>
                          </Card>
                        );
                      }
                    )}
                  </CardContent>
                </Card>
              </>
            )}
          </CardContent>
        </Card>
      </div>
    </div>
  );
}

export default App;
